@extends("templates.serveur")
@section("content")    
    @isset($categories)
        <div class="container mb-3">
            <div class="row  d-flex justify-content-between">
                @foreach($categories as $categorie)
                    <div class="col-2 d-flex flex-column align-content-center flex-wrap">
                        <img src="{{asset($categorie->photo)}}" alt="" width="60px" class="small-img ms-auto">
                        <a href="{{route('dashboard.categorie.show', $categorie->id)}}" class="link-secondary fs-6 text-start" >{{$categorie->titre}}</a>
                    </div>
            @endforeach
        </div>
        </div>
    @endisset
    @isset($plats)
        <div class="row d-flex flex-wrap mt-3 mx-auto justy-content-around">
            @foreach($plats as $plat)
                @php
                    $isset_commande=Session::has("commande");
                    $in_commande=false;
                        if( $isset_commande)
                        {
                            $commande=Session("commande");
                            $in_commande=$commande->plats()->find($plat->id)!==null;
                        } 
                            
                @endphp
                <div class="card  col-3"  >
                    <img src="{{asset($plat->getPhoto())}}" class="medium-img" alt="..." >
                    <div class="card-body text-center">
                        <h6 class="card-title">{{$plat->intitule}}</h6>
                        <p class="card-text">{{$plat->prix}}</p>
                      @if($isset_commande && !$in_commande) 
                      <form action="{{route('serveur.commande.add_plat')}}" method="post">
                        @csrf
                        <input type="hidden" name="plat_id" value="{{$plat->id}}">
                        <input type="number" value="1" name="nombre" min="1" style="width:100px"/>
                        <button type="submit" class="btn btn-success fs-6"><i class="bi bi-plus-circle"></i></button>
                      </form>
                        @endif
                        </div>
                    </div>
            @endforeach
        </div>
    @endisset
@endsection