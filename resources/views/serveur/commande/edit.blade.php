@extends("templates.serveur")
@section("content")  

<div class="row">
         
<form action="{{route('commande.update', $commande->id)}}" method="post" class="mx-3" >
    @csrf
    @method("PUT")
<table class="table table-striped">    
    @foreach($commande->plats as $plat)
     <tr>
        <td>
            <input type="checkbox" value="{{$plat->id}}" name="plats[]"/>
        </td>
        <td class="w-5">
            {{$plat->intitule}}         
        </td>
        <td>
            <input type="number" value="{{$plat->pivot->nombre}}" name="Ids[{{$plat->id}}]" size="3" min="1"/>
        </td>
     </tr>
        @endforeach
        <tr>
            <td colspan="3" style="text-align:right">
                    <p>Total: <b>{{$commande->total()}} Dhs</b></p>
            </td>
        </tr>
    
</table>
 <button type="submit" class="btn btn-danger" name="supprimer">Supprimer</button>
             <button type="submit" class="btn btn-success" name="modifier">Mettre à jour</button>
             <a href="{{route('dashboard')}}" class="btn btn-primary">Ajouter un autre plat</a>

    </form>   
</div>
@endsection