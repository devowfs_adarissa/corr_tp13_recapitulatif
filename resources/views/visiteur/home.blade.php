@extends("templates.visiteur")
@section("titre", "Home")
@section("contenu")
   <div class="offset-md-1 row">
    
        @isset($categories)
            @foreach($categories as $categorie)
         <div class="col-md-5">  
             <div class="card mb-3">
                <img src="{{ asset($categorie->photo) }}" class="card-img-top" alt="{{ $categorie->titre }}" width="80%">
                <div class="card-body">
                    <h5 class="card-title">{{ $categorie->titre }}</h5>
                    <p class="card-text"> {{ $categorie->description }}</p>
                      <a href="{{ route('plats_Categorie', $categorie->id)}}" class="btn btn-primary">Découvrir les {{ $categorie->plats->count() }} plats </a>
                    </p>
                    <!-- <p class="card-text"><small class="text-body-secondary">Last updated 3 mins ago</small></p> -->
                </div>
            </div>
             </div>
            @endforeach
        @endisset

</div>
@endsection
