<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('titre')</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <style>
      .small-img{
        
           width: 60px; /* You can set the dimensions to whatever you want */
            height: 60px;
            object-fit: cover;
            display: block;
     
      }
       .medium-img{
         width: 150px; /* You can set the dimensions to whatever you want */
            height: 150px;
            object-fit: cover;
            display: block;
            margin:auto;
       }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary d-flex justyfy-content-between">
  <div class="container-fluid">
    <div class="card">
      <a class="navbar-brand" href="{{route('home')}}">Mon Resto <br>
                @auth
              <span class="fs-6"> Serveur : {{Auth::user()->name}}</span> 
                @endauth
          </a>
   <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
   </div>
    
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mx-auto mb-2 mb-lg-0 ">
        <li class="nav-item">
          <a  aria-current="page" href="{{route('dashboard')}}" class=" nav-link {{(Route::is('dashboard'))? 'active':''}}">Dashboard</a>
        </li>
        <li class="nav-item">
          <a  href="{{route('commande.index')}}" class="nav-link {{(Route::is('commande.index'))? 'active':''}}">Commandes</a>
        </li>
      
      </ul>
      <div>
        @if(!Session::has("commande"))
          <a href="{{route('serveur.commande.creer')}}" class="btn btn-primary">Nouvelle commande</a>
        @else
          @php
            $commande=Session::get("commande");
            $nombrePlats=$commande->plats()->count();
          @endphp
          <span>Commande créée</span>
          <a href="{{route('commande.edit', $commande->id)}}" class="btn btn-success">Editer</a>
          <a href="{{route('commande.terminer')}}" class="btn btn-primary">Terminer ({{ $nombrePlats }})</a>
        @endif
      </div>
    </div>
  </div>
</nav>
    <div class="container mt-3 mb-5">
        @yield('content')
    </div>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>