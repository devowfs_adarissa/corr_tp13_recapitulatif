<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @isset($plat)
    <h2>La composition du plat {{ $plat->intitule }}</h2>
    @endisset
    @isset($compositons)
    <table>
        <tr>
            <th>id</th>
            <th>libelle</th>
            <th>Quté</th>
            <th>Unité</th>
        </tr>
        @foreach($compositons as $composition)
        <tr>
            <td>{{ $composition->id }}</td>
            <td>{{ $composition->libelle }}</td>
            <td>{{ $composition->pivot->quantite }}</td>
            <td>{{ $composition->pivot->unite }}</td>
        </tr>
        @endforeach
    </table>
    @endisset
</body>
</html>