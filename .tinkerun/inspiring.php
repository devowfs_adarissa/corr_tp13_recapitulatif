<?php

use Illuminate\Foundation\Inspiring;
use App\Models\Categorie;
use App\Models\Plat;
use App\Models\Commande;
use App\Models\Serveur;
use Illuminate\Database\Eloquent\Builder;

Inspiring::quote();

// $now=now();
// $hier= $now->subDay(1);

// $serveur=Serveur::find(2);
// $serveur->withCount(["commandes"=> function(Builder $query) use($hier){
//     $query->whereDate("created_at", $hier);
// }])->get();


// Plat::with("commandes",function($query){
//    return $query->pivot->nombre;
// })->get();

Plat::with("commandes:pivot.nombre")->get();