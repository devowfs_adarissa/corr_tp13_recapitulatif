<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\serveur\CommandeController;
use App\Http\Controllers\Visiteur\CategorieController;
use App\Http\Controllers\Serveur\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route("home");
});
Route::get("/home", [CategorieController::class, "index"])->name('home');
Route::get("/plats/{categorie}", [CategorieController::class, "plats"])->name("plats_Categorie");


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
Route::middleware("auth")->group(function(){
    Route::get("dashboard", [DashboardController::class, "index"])->name("dashboard");
    Route::get("dashboard/categorie/{categorie}", [DashboardController::class, "show"])->name("dashboard.categorie.show");
    Route::get("serveur/commande/creer", [CommandeController::class, "creer"])->name("serveur.commande.creer");
    // Route::get("serveur/commande/{commande}/edit", [CommandeController::class, "edit"])->name("serveur.commande.edit");
 Route::get("serveur/commande/terminer/{commande?}", [CommandeController::class, "terminer"])->name("commande.terminer");
    // Route::post("serveur/commande/{commande}/update", [CommandeController::class, "update"])->name("serveur.commande.update");

    Route::resource("seveur/commande", CommandeController::class);

    Route::post("serveur/plat/add", [CommandeController::class, "add_plat"])->name("serveur.commande.add_plat");
    Route::get("serveur/commande/{commande}/{etat}", [CommandeController::class, "changerEtat"])->name("serveur.commande.changer_etat");

});

