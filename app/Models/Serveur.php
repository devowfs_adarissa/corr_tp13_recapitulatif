<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Serveur extends Model
{
    use HasFactory;
    public function commandes(){
        return $this->hasMany(Commande::class);
    }

    public function commandesEncours(){
        return $this->commandes()->where("etat", "en_cours");
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
