<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;

    protected $guarded=[];

    public function serveur(){
        return $this->belongsTo(Serveur::class);
    }

    public function plats(){
        return $this->belongsToMany(Plat::class)->withPivot("nombre");
    }


    public function total(){
        return $this->plats->sum(function($plat){
            return $plat->pivot->nombre*$plat->prix;
        });
    }
}
