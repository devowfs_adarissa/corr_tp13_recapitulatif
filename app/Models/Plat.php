<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plat extends Model
{
    use HasFactory;

    public function categorie(){
        return $this->belongsTo(Categorie::class);
    }

    public function composants(){
        return $this->belongsToMany(Composant::class)
                    ->withPivot(["quantite", "unite"]);
    }

      public function commandes(){
        return $this->belongsToMany(Commande::class)->withPivot("nombre");
    }

    public function getPhoto(){
        return $this->photo?? "plats/default.png";
    }
     public function lesPlusCommandes(int $nombre)
    {
       $query=DB::raw("select p.id, p.intitule, p.description, p.prix, p.photo, sum(cp.nombre) as nombre_commandes from plats p, commande_plat cp where p.id=cp.plat_id group by p.id, p.intitule, p.description, p.prix, p.photo order by nombre_commandes DESC");
       return DB::select($query);
    }
}
