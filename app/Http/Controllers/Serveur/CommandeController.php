<?php

namespace App\Http\Controllers\serveur;

use App\Http\Controllers\Controller;
use App\Models\Commande;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if(Auth::check()){
            $user_id=Auth::id();
            $commandes=Commande::where("serveur_id",$user_id)
            ->orderBy("created_at", "DESC")
            ->get();
            return view("serveur.commande.index", compact("commandes"));
        }
       
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function creer()
    {
        $commande=Commande::create(
            [
                "serveur_id"=>Auth::id(),
                "etat"=>"en_cours",
                "paye"=>false
            ]
            );
       Session::put('commande', $commande);
         return redirect()->route("dashboard");
    }

    public function add_plat(Request $request){
        $commande=Session::get('commande');
        $commande->plats()->attach($request->plat_id, ["nombre"=>$request->nombre]);

     return redirect()->back();
    }
    /**
     * Display the specified resource.
     */
    public function show(Commande $commande)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Commande $commande)
    {
        return view("serveur.commande.edit", compact("commande"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Commande $commande)
    {
        if($request->has("supprimer")){
            if(isset($request->plats)){
                foreach($request->plats as $id){
                $commande->plats()->detach($id);
                 }
            }
            
        }elseif($request->has("modifier")){
            $tab=$request->Ids;
            if(isset($tab)){
                 foreach( $tab as $id=>$nombre){
                    $commande->plats()->updateExistingPivot($id, ["nombre"=>$nombre]);
                }
            }
          
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Commande $commande)
    {
        //
    }

    public function terminer(Commande $commande=null){
        if($commande==null && Session::has('commande'))
        {   
            $commande=Session::get('commande');
            Session::forget("commande"); 
        }
            if($commande->plats()->count()==0)
                $commande->delete();
            else{
                $commande->etat="terminé";
                $commande->save();
                }
            
            
       
        return redirect()->route("dashboard");
    }

    public function changerEtat(Commande $commande, string $etat){
            $commande->update(["etat"=>$etat]);
            return redirect()->back();
    }
}
