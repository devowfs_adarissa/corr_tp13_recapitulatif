<?php

namespace App\Http\Controllers\serveur;

use App\Http\Controllers\Controller;
use App\Models\Categorie;
use App\Models\Plat;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $categories= Categorie::all();
        $plats=Plat::all();
        return view("serveur.dashboard", compact("categories", "plats"));
    }

    public function filter(Request $request){
        
    }

    public function show(Categorie $categorie){
        $categories=Categorie::all();
       $plats=$categorie->plats;
        return view("serveur.dashboard", compact("categories", "plats"));
    }
}
