<?php

namespace App\Http\Controllers\Visiteur;

use App\Http\Controllers\Controller;
use App\Models\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
     public function index(){
        $categories=Categorie::all();
        
        return view("visiteur.home", compact("categories"));
    }

    public function plats($id){
      $categorie=Categorie::find($id);
      $plats=  $categorie->plats;
      return view("visiteur.plats", compact("plats", "categorie"));
      
    }
}
